FROM mysql

ENV MYSQL_ROOT_PASSWORD saiku
ENV MYSQL_DATABASE foodmart_mondrian

COPY ./scripts/ /docker-entrypoint-initdb.d/

RUN touch /var/run/mysqld/mysqld.sock
RUN touch /var/run/mysqld/mysqld.pid
RUN chown -R mysql:mysql /var/run/mysqld/mysqld.sock
RUN chown -R mysql:mysql /var/run/mysqld/mysqld.pid

EXPOSE 3306